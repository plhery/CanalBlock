CanalBlock
==========
Bloque les pubs du site canal+, et rend le chargement plus rapide

LICENSE : Apache V2.

==========
INSTALLATION : 
- sur https://chrome.google.com/webstore/detail/canalblock/pmfiidlmhkgloocnlnomcgfdgpoabigo

- via le .crx à télecharger sur https://github.com/MrPointVirgule/CanalBlock/releases/ puis à glisser dans chrome://extensions/

- télechargez les fichiers de github, allez dans chrome://extensions/, cliquez sur "mode développeur", "charger l'extension non empaquetée..", et selectionnez le dossier dans lequel vous avez les fichiers github
